#!/bin/bash
date_today=`date +%Y-%m-%d`
>/var/www/html/index.html
curl -s https://date.nager.at/Api/v2/AvailableCountries |jq -r '.[] | [.key, .value] | @csv'|sed 's/\"//g' > list_of_countries.txt
is_today_holiday=`curl -s https://date.nager.at/Api/v2/NextPublicHolidaysWorldwide | jq -r  ".[0] |.date" |grep $date_today`


if [ -z "$is_today_holiday" ]
then
cat << EOF >> /var/www/html/index.html
<!DOCTYPE html>
<html>
<header>
<title>Hello world</title>
</header>
<body>
<p>Hello world !!!</p>
<p>Today is $date_today</p>
</body>
</html>
EOF

else

country_code=`curl -s https://date.nager.at/Api/v2/NextPublicHolidaysWorldwide | jq -r  ".[0] |[.date,.countryCode]| @csv" |grep $date_today|sed 's/\"//g' |awk -F "," '{print $2}'`
country=`grep $country_code list_of_countries.txt |awk -F "," '{print $2}'`

cat << EOF >> /var/www/html/index.html
<!DOCTYPE html>
<html>
<header>
<title>Hello world</title>
</header>
<body>
<p>Hello world !!!</p>
<p>Today is $date_today</p>
<p>Today is a holiday in $country</p>
</body>
</html>
EOF

fi
